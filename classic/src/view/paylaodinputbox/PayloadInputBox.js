/**
 * This view is an example list of people.
 */
Ext.define('BoatCrash.view.payloadinputbox.PayloadInputBox', {
    extend: 'Ext.container.Container',
    xtype: 'PayloadInputBox',
    scrollable: false,
    margin: '10 10 10 10',
    controller: 'PayloadInputBox',
    viewModel: 'PayloadInputBoxModel',
    requires: [
        'BoatCrash.view.payloadinputbox.PayloadInputBoxController',
        'BoatCrash.view.payloadinputbox.PayloadInputBoxModel'
    ],

    defaults: {
        border: true
    },
    items: [
        {
            xtype: 'container', 
            layout: 'vbox', 
            items: [
                {
                    xtype: 'label',
                    id: 'jsonBattelship',
                    style: "font-size:xx-large; font-style: italic; font-weight:bold;",
                    margin: '5 20 20 10',
                    bind: {
                        html: '{JSONBattelship}'
                    }
                },
                {
                    xtype: 'label',
                    id: 'payloadOutput',
                    style: "font-size:small;",
                    margin: '20 20 10 0',
                    bind: {
                        html: 'Payload: ' + '{finalPayload}'
                    }
                },
                {
                    xtype: 'container', 
                    layout: 'hbox',
                    align: 'bottom',
                    items: [
                        {
                            xtype: 'textfield',
                            id: 'payloadbox',
                            bind: {
                                value: '{payloadInput}',
                                emptyText: '{payloadInputplaceholder}'
                            },
                            width: '340px'
                        }, 
                        {
                            xtype: 'button',
                            id: 'setpayloadbtn',
                            text: 'Plant',
                            style: 'align:center; background-color: #1B5297; border-radius: 6px; text-color: white;',
                            margin: '0 0 0 10',
                            padding: '7, 7, 7, 7 ',
                            listeners: {
                                click: 'setBomb'
                            }
                        }
                    ]
                },
                {
                    xtype: 'label',
                    id: 'gameStatusMessage',
                    style: "font-weight:bold; font-size:medium;",
                    margin: '20 20 20 0',
                    bind: {
                        html: '{JSONBattelshipHintMessage}'
                    }
                },
            ]
        }
    ]
});