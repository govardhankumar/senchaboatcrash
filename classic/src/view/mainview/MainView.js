Ext.define('BoatCrash.view.mainview.MainView', {
    extend: Ext.container.Container,
    xtype: 'MainView',
    requires: [
        'BoatCrash.view.payloadinputbox.PayloadInputBox',
        'BoatCrash.view.xsslevel6.XssLevel6'
    ],
    beforeRender: function()
    {
        console.log('boatcrashdiv ' + Ext.Element.get('boatcrashdiv'));
    },
    config: {
        layout: {
            type: 'hbox',
            align: 'center',
            pack : 'center'
        },
        items: [
            {
               xtype: 'PayloadInputBox'
            },
            {
                xtype: 'XssLevel6'
            }
        ]
    }
});