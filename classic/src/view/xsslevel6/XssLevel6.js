/**
 * This view is an example list of people.
 */
Ext.define('BoatCrash.view.xsslevel6.XssLevel6', {

    extend: 'Ext.container.Container',
    xtype: 'XssLevel6',
    controller: 'XssLevel6',
    id: 'pageTable',
    requires: [
        'BoatCrash.view.xsslevel6.XssLevel6Controller'
    ],
    scrollable: false,
    
    margin: '10 10 10 10',
    defaults: {
        //bodyStyle: 'background: #FFEEEEEE;',
        split: true,
        border: true
    },
    layout: {
        type: 'table',
        pack: 'start',
        columns: 3,
        tableAttrs: {
            style: {
                margin: 'auto'
            }
        }
    },
    
    items: [{
        id: 'position1',
        html: '<div style="padding: 10px;"><b>1<b></div>',
        height: 120,
        width: 120
    }, {
        id: 'position2',
        html: '<div style="padding: 10px;"><b>2<b></div>',
        height: 120,
        width: 120
        
    }, {
        id: 'position3',
        html: '<div style="padding: 10px;"><b>3<b></div>',
        height: 120,
        width: 120
    }, {
        id: 'position4',
        html: '<div style="padding: 10px;"><b>4<b></div>',
        height: 120,
        width: 120
    }, {
        id: 'position5',
        html: '<div style="padding: 10px;"><b>5<b></div>',
        height: 120,
        width: 120
    }, {
        id: 'position6',
        html: '<div style="padding: 10px;"><b>6<b></div>',
        height: 120,
        width: 120
    }, {
        id: 'position7',
        html: '<div style="padding: 10px;"><b>7<b></div>',
        height: 120,
        width: 120
    }, {
        id: 'position8',
        html: '<div style="padding: 10px;"><b>8<b></div>',
        height: 120,
        width: 120
    }, {
        id: 'position9',
        html: '<div style="padding: 10px;"><b>9<b></div>',
        height: 120,
        width: 120
    }]
});