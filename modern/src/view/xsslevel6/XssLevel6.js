/**
 * This view is an example list of people.
 */
Ext.define('BoatCrash.view.xsslevel6.XssLevel6', {
    extend: 'Ext.panel.Panel',
    xtype: 'XssLevel6',
    controller: 'XssLevel6',
    requires: [
        'BoatCrash.view.xsslevel6.XssLevel6Controller'
    ],
    width:400,
    scrollable: true,
    defaults: {
        bodyStyle: 'background: #FFEEEEEE;',
        border: true
    },

    layout: {
        type: 'table',
        align: 'left',
        columns: 3,
        tableAttrs: {
            style: {
                margin: 'auto'
            }
         }
    },
    
    items: [{
        id: 'position1',
        html: '<div style="height: 200px; width: 200px; text-align: left; padding: 10px;"><b>1<b></div>',
        height: 200,
        width: 200
    }, {
        id: 'position2',
        html: '<div style="height: 200px; width: 200px; text-align: left; padding: 10px;"><b>2<b></div>',
        height: 200,
        width: 200
        
    }, {
        id: 'position3',
        html: '<div style="height: 200px; width: 200px; text-align: left; padding: 10px;"><b>3<b></div>',
        height: 200,
        width: 200
    }, {
        id: 'position4',
        html: '<div style="height: 200px; width: 200px; text-align: left; padding: 10px;"><b>4<b></div>',
        height: 200,
        width: 200
    }, {
        id: 'position5',
        html: '<div style="height: 200px; width: 200px; text-align: left; padding: 10px;"><b>5<b></div>',
        height: 200,
        width: 200
    }, {
        id: 'position6',
        html: '<div style="height: 200px; width: 200px; text-align: left; padding: 10px;"><b>6<b></div>',
        height: 200,
        width: 200
    }, {
        id: 'position7',
        html: '<div style="height: 200px; width: 200px; text-align: left; padding: 10px;"><b>7<b></div>',
        height: 200,
        width: 200
    }, {
        id: 'position8',
        html: '<div style="height: 200px; width: 200px; text-align: left; padding: 10px;"><b>8<b></div>',
        height: 200,
        width: 200
    }, {
        id: 'position9',
        html: '<div style="height: 200px; width: 200px; text-align: left; padding: 10px;"><b>9<b></div>',
        height: 200,
        width: 200
    }]
});