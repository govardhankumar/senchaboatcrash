Ext.define('BoatCrash.view.mainview.MainView', {
    xtype: 'MainView',
    extend: 'Ext.container.Container',
    requires: [
        'BoatCrash.view.payloadinputbox.PayloadInputBox',
        'BoatCrash.view.xsslevel6.XssLevel6'
    ],
    
    config: {
        layout: {
            type: 'vbox',
        },
        items: [
            {
                xtype: 'PayloadInputBox'
            },
            {
                xtype: 'XssLevel6'
            }
        ]
    }
});