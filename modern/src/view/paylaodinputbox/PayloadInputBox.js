/**
 * This view is an example list of people.
 */
Ext.define('BoatCrash.view.payloadinputbox.PayloadInputBox', {
    extend: 'Ext.container.Container',
    xtype: 'PayloadInputBox',
    scrollable: false,
    defaults: {
        bodyStyle: 'background: #FFEEEEEE;',
        border: true
    },
    
    items: [
        {
            
            xtype: 'container', 
            layout: 'vbox', 
            items: [
                {
                    xtype: 'textfield'
                }, 
                {
                    xtype: 'button',
                    text: 'test'
                }
            ]
        }
    ]
});