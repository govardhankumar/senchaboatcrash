/**
 * This class is the view model for the Main view of the application.
 */
Ext.define('BoatCrash.view.xsslevel6.XssLevel6Model', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.Xsslevel6',
    data: {
        payload: '[{"name":"bomb1","cell_number":"2"},{"name":"bomb2","cell_number":"6"}]'
    }
});
