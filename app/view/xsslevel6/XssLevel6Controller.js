/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('BoatCrash.view.xsslevel6.XssLevel6Controller', {
    extend: 'Ext.app.ViewController',
 
    requires: [
        'BoatCrash.view.xsslevel6.XssLevel6Model',
    ],

    alias: 'controller.XssLevel6',
    view: 'XssLevel6',
    viewModel: 'XssLevel6',

    init: function () {
        var me = this;
        me.initBoat();
        me.listen({
            controller: {
                '*': {
                    setBomb: 'setBomb'
                }
            }
        });
    },

    initBoat: function() {
        var randomBoatPosition  = Math.round(Math.random() * 8) + 1;
        Ext.getCmp('position' + randomBoatPosition).setHtml('<div class="ravan-boat"><b>' + randomBoatPosition + '<b></div>');
    },

    setBomb: function(payloadStr) {
        var gameWonStatus = false;
        var payload = JSON.parse(payloadStr);

        //var payload = [{"cell_number":"2"},{"cell_number":"6"}]<img src=x onerror=flag()>;
        
        var randomBoatPosition  = Math.round(Math.random() * 8) + 1;
 
        var bomb1Pos = payload[0].cell_number;
        var bomb2Pos = payload[1].cell_number;
        console.log('bomb1 ' + bomb1Pos);
        console.log('bomb2 ' + bomb2Pos);
        console.log('random position ' + randomBoatPosition);
 
        this.resetCrash();

        if(bomb1Pos == randomBoatPosition)
        {
            gameWonStatus = true;
            Ext.getCmp('position' + randomBoatPosition).setHtml('<div class="ravan-exploided-bomb"><b>' + randomBoatPosition + '<b></div>');
            Ext.getCmp('position' + bomb2Pos).setHtml('<div class="ravan-bomb"><b>' + bomb2Pos + '<b></div>');
        }
        else if(bomb2Pos == randomBoatPosition)
        {
            gameWonStatus = true;
            Ext.getCmp('position' + randomBoatPosition).setHtml('<div class="ravan-exploided-bomb"><b>' + randomBoatPosition + '<b></div>');
            Ext.getCmp('position' + bomb1Pos).setHtml('<div class="ravan-bomb"><b>' + bomb1Pos + '<b></div>');
        }
        else
        {
            Ext.getCmp('position' + randomBoatPosition).setHtml('<div class="ravan-boat"><b>' + randomBoatPosition + '<b></div>');
            Ext.getCmp('position' + bomb1Pos).setHtml('<div class="ravan-bomb"><b>' + bomb1Pos + '<b></div>');
            Ext.getCmp('position' + bomb2Pos).setHtml('<div class="ravan-bomb"><b>' + bomb2Pos + '<b></div>');
        }

        this.fireEvent('setGameStatusMessage', gameWonStatus);
    },
    
    resetCrash: function() {
        for(var colNo = 1; colNo <= 9 ; colNo++)
        {
            Ext.getCmp('position' + colNo).setHtml('<div style="padding: 10px;"><b>' + colNo + '<b></div>');
        }
    }
 });
