/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('BoatCrash.view.payloadinputbox.PayloadInputBoxController', {
    extend: 'Ext.app.ViewController',
    view: 'PayloadInputBox',
    alias: 'controller.PayloadInputBox',

    init: function() {
        Ext.getCmp('gameStatusMessage').setHtml(this.getViewModel().get('JSONBattelshipHintMessage'));
        var me = this;
        me.listen({
            controller: {
                '*': {
                    setGameStatusMessage: 'setGameStatusMessage'
                }
            }
        });
    },

    setBomb: function() {
        this.getViewModel().set('finalPayload', this.getViewModel().get('payloadInput'));
        this.fireEvent('setBomb', this.getViewModel().get('finalPayload'));
    },

    setGameStatusMessage: function(gameWonStatus) {
        console.log('setGameStatusMessage method called');
        Ext.getCmp('gameStatusMessage').setHtml(this.getViewModel().get(gameWonStatus ? 'JSONBattelshipWonMessage' : 'JSONBattelshipLostMessage'));
    }
 });
