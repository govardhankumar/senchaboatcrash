/**
 * This class is the view model for the Main view of the application.
 */
Ext.define('BoatCrash.view.payloadinputbox.PayloadInputBoxModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.PayloadInputBoxModel',
    
    data: {
        payloadInput: '',
        finalPayload: '',
        JSONBattelship: 'JSON Battleship',
        //JSONBattelship: window.$Label.JSONBattleshipTitle,
        JSONBattelshipHintMessage: '&#128070; Plant your Bombs &#128070;',
        JSONBattelshipLostMessage: '&#128544; You lost the battles &#128544;',
        JSONBattelshipWonMessage: '&#128519; Wao, you won the battle &#128519;',
        payloadInputplaceholder: '[{\u201Ccell_number\u201C:\u201C2\u201C},{\u201Ccell_number\u201C:\u201C6\u201C}]'
    }
});
