/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('BoatCrash.view.main.MainController', {
    extend: 'Ext.app.ViewController',
 
    alias: 'controller.Main',

    init: function() {
        console.log('Form init function');
        
    }
 });
